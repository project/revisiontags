<?php
// $Id $


/**
 * Implementation of hook_views_data()
 */
function revisiontags_views_data() {
    // ----------------------------------------------------------------------
  // revisiontags table

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this field by default.
  $data['revision_tags']['table']['group']  = t('Revisiontags');

  // Advertise this table as a possible base table
  $data['revision_tags']['table']['base'] = array(
    'field' => 'vid',
    'title' => t('Revisiontags'),
    'help' => t('Tags added to Node revisions.'),
  );

  // For other base tables, explain how we join
  $data['revision_tags']['table']['join'] = array(
    // Directly links to node table.
    'node_revisions' => array(
      'left_table' => 'node_revisions', // Because this is a direct link it could be left out.
      'left_field' => 'vid',
      'field' => 'vid',
      'table' => 'revision_tags',
    ),
  );

  // vid field
  $data['revision_tags']['vid'] = array(
    'title' => t('Vid'),
    'help' => t('The revision ID of the node revision.'), // The help that appears on the UI,
    // Information for displaying the nid
    'field' => array(
      'field' => 'vid',
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
    ),
    // Information for accepting a vid as an argument
    'argument' => array(
      'handler' => 'views_handler_argument_node_vid',
      'parent' => 'views_handler_argument_numeric', // make sure parent is included
      'click sortable' => TRUE,
      'numeric' => TRUE,
    ),
    // Information for accepting a nid as a filter
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    // Information for sorting on a nid.
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // tag field
  $data['revision_tags']['tag'] = array(
    'title' => t('Revisiontag'), // The item it appears as on the UI,
    'help' => t('The revision tag of the revision.'), // The help that appears on the UI,
     // Information for displaying a tag as a field
    'field' => array(
      'handler' => 'revisiontags_handler_field_tag',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // public field
  $data['revision_tags']['public'] = array(
    'title' => t('Public'),
    'help' => t('Is the revisiontag public.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Published'),
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // Title field = node_revisions.title but with revisions view link.
    // title
  $data['node_revisions']['tag_title'] = array(
    'title' => t('Title'), // The item it appears as on the UI,
    'group' => t('Revisiontags'),
    'help' => t('The title of the tagged revision.'), // The help that appears on the UI,
     // Information for displaying a title as a field
    'field' => array(
      'field' => 'title', // the real field
      'group' => t('Revisiontags'),
      'handler' => 'revisiontags_handler_field_node_revision_link_view',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  $data['node_revisions']['tag_timestamp'] = array(
    'title' => t('Created date'), // The item it appears as on the UI,
    'group' => t('Revisiontags'),
    'help' => t('The date the node revision was created.'), // The help that appears on the UI,
    'field' => array(
      'field' => 'timestamp', // the real field
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );


  // Nid field = needed for sort.
    // title
  $data['node_revisions']['nid'] = array(
    'title' => t('Nid'), // The item it appears as on the UI,
    'group' => t('Revisiontags'),
    'help' => t('The nid of the node.'), // The help that appears on the UI,
     // Information for displaying a title as a field
    'field' => array(
      'field' => 'node', // the real field
      'group' => t('Revisiontags'),
      'handler' => 'views_handler_field_node',
      'click sortable' => TRUE,
     ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
/*
  $data['node_revisions']['uid'] = array(
    'title' => t('User'),
    'help' => t('Relate a node revision to the user who created the revision.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'field' => 'uid',
      'label' => t('user'),
    ),
  );
*/


/* @TODO: later, when I understand how to do this 
  $data['revision_tags']['edit_tag'] = array(
    'field' => array(
      'title' => t('Edit revisiontag'),
      'help' => t('Provide a simple link to edit revisiontag.'),
      'handler' => 'revisiontags_handler_field_revisiontags_edit',
    ),
  );

  $data['revision_tags']['delete_tag'] = array(
    'field' => array(
      'title' => t('Delete revisiontag'),
      'help' => t('Provide a simple link to delete the revisiontag.'),
      'handler' => 'revisiontags_handler_field_revisiontags_edit',
    ),
  );
*/

  // For other base tables, explain how we join
  $data['node_revisions']['table']['join'] = array(
    'revision_tags' => array(
      'handler' => 'views_join', // this is actually optional
      'left_table' => 'revision_tags', // Because this is a direct link it could be left out.
      'left_field' => 'vid',
      'field' => 'vid',
     ),
  );
  
//  print '<pre>'. var_export($data, true) .'</pre>';
  return $data;
}

/**
 * Implementation of hook_views_handlers() to register all of the basic handlers
 * views uses.
 */
function revisiontags_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'revisiontags') . '/views',
    ),
    'handlers' => array(
      // field handlers
      'revisiontags_handler_field_node_revision_link_view' => array(
        'parent' => 'views_handler_field'
      ),
      'revisiontags_handler_field_tag' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

