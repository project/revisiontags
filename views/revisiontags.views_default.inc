<?php
// $Id $

/**
 * Implementation of hook_default_view_views().
 */
function revisiontags_views_default_views() {
  $view = new view;
  $view->name = 'revision_tags';
  $view->description = 'A list of all nodes with revisiontags';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'revision_tags';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = TRUE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'tag_timestamp' => array(
      'label' => 'Revision date',
      'date_format' => 'small',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'tag_timestamp',
      'table' => 'node_revisions',
      'field' => 'tag_timestamp',
      'relationship' => 'none',
    ),
    'tag_title' => array(
      'label' => 'Title',
      'exclude' => 0,
      'text' => '',
      'id' => 'tag_title',
      'table' => 'node_revisions',
      'field' => 'tag_title',
      'relationship' => 'none',
      'link_to_node' => 1,
    ),
    'tag' => array(
      'label' => 'Revisiontag',
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'tag',
      'table' => 'revision_tags',
      'field' => 'tag',
      'relationship' => 'none',
    ),
    'public' => array(
      'label' => 'Public',
      'type' => 'yes-no',
      'not' => 0,
      'exclude' => 0,
      'id' => 'public',
      'table' => 'revision_tags',
      'field' => 'public',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'nid' => array(
      'order' => 'ASC',
      'id' => 'nid',
      'table' => 'node_revisions',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'vid' => array(
      'order' => 'ASC',
      'id' => 'vid',
      'table' => 'revision_tags',
      'field' => 'vid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'view tagged revisions',
  ));
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'timestamp' => 'timestamp',
      'tag_title' => 'tag_title',
    ),
    'info' => array(
      'timestamp' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'tag_title' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'revisiontags');
  $handler->override_option('menu', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'weight' => 0,
  ));
  $views[$view->name] = $view;

  return $views;
}
