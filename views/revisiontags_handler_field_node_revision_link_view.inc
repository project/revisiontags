<?php
/**
 * @file
 * Contains the basic 'node' field handler.
 */

/**
 * Field handler to provide simple renderer that allows linking to a node.
 */
class revisiontags_handler_field_node_revision_link_view extends views_handler_field {
  /**
   * Constructor to provide additional field to add.
   */
  function construct() {
    parent::construct();
    $this->additional_fields['nid'] = 'nid';
    $this->additional_fields['node_vid'] = array('table' => 'node_revisions', 'field' => 'vid');
    $this->additional_fields['node_title'] = array('table' => 'node_revisions', 'field' => 'title');
    $this->additional_fields['vid'] = 'vid';
  }

  function access() {
    return user_access('view revisions') || user_access('administer nodes');
  }

  function option_definition() {
    $options = parent::option_definition();
    $options['link_to_node'] = array('default' => FALSE);
    return $options;
  }

  /**
   * Provide link to node option
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['link_to_node'] = array(
      '#title' => t('Link this field to its node'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->options['link_to_node']),
    );
  }

  /**
   * Render whatever the data is as a link to the node.
   *
   * Data should be made XSS safe prior to calling this function.
   */
  function render($values) {
    // ensure user has access to view this node.
    $node = new stdClass();
    $node->nid = $values->{$this->aliases['nid']};
    $node->vid = $values->{$this->aliases['node_vid']};
    $node->title = $values->{$this->aliases['node_title']};

    $text = check_plain($node->title);
    return l($text, "node/$node->nid/revisions/$node->vid/view");
  }
}
